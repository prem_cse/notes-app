# NOTES APP



## Project Overview

This is the Assignment shared by Internshala.



This app uses the local Room database for performing CRUD operations.




## What I learned

- Load data from the locaI Room database.

- Use adapters and custom list layouts to populate recycler views

- Incorporate libraries to simplify the amount of code you need to write

- Build a fully featured application that looks and feels natural on the latest Android operating system.



## Libraries

- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)

* [Room](https://developer.android.com/topic/libraries/architecture/room)

* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)

* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)

- [Android Data Binding](https://developer.android.com/topic/libraries/data-binding/)


## Demo
- [Video link](https://drive.google.com/file/d/1c4FtptcHA-54S_EAFq-ZUeCdikAY5kbn/view?usp=sharing)
- [APK](https://drive.google.com/file/d/12rPgdQouv9f46-FmN9txdP5iMb3f19Rs/view?usp=sharing)
