package com.example.notesapp.data;

import androidx.room.TypeConverter;
import org.jetbrains.annotations.NotNull;
import com.example.notesapp.data.models.Priority;

public final class Converter {
    @TypeConverter
    @NotNull
    public final String fromPriority(@NotNull Priority priority) {
        return priority.name();
    }

    @TypeConverter
    @NotNull
    public final Priority toPriority(@NotNull String priority) {
        return Priority.valueOf(priority);
    }
}
