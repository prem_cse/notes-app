package com.example.notesapp.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.notesapp.data.models.Note;

import java.util.List;

@Dao
public interface NoteDao {


    @Query("SELECT * FROM notes_table ORDER BY id ASC")
    LiveData<List<Note>> getAllNotes();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertData(Note note);

    @Update
    void updateData(Note note);

    @Delete
    void deleteItem(Note note);

    @Query("DELETE FROM notes_table")
     void deleteAll();

    @Query("SELECT * FROM notes_table WHERE title LIKE :searchQuery")
    LiveData<List<Note>> searchDatabase(String searchQuery);

    @Query("SELECT * FROM notes_table ORDER BY CASE WHEN priority LIKE 'H%' THEN 1 WHEN priority LIKE 'M%' THEN 2 WHEN priority LIKE 'L%' THEN 3 END")
    LiveData<List<Note>> sortByHighPriority();

    @Query("SELECT * FROM notes_table ORDER BY CASE WHEN priority LIKE 'L%' THEN 1 WHEN priority LIKE 'M%' THEN 2 WHEN priority LIKE 'H%' THEN 3 END")
    LiveData<List<Note>> sortByLowPriority();

}
