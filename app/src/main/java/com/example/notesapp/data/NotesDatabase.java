package com.example.notesapp.data;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.notesapp.data.models.Note;

import static com.example.notesapp.utils.Constant.DATABASE_NAME;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
@TypeConverters({Converter.class})
public abstract class NotesDatabase extends RoomDatabase {
    private static final String TAG = NotesDatabase.class.getSimpleName();

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static NotesDatabase sInstance;

    public static NotesDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                Log.d(TAG, "Creating new database instance");
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        NotesDatabase.class, DATABASE_NAME)
                        .build();
            }
        }
        Log.d(TAG, "Getting the database instance");
        return sInstance;
    }

    // The associated DAOs for the database
    public abstract NoteDao notesDao();
}
