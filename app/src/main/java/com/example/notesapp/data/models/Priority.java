package com.example.notesapp.data.models;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW
}
