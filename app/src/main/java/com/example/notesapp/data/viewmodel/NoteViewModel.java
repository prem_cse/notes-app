package com.example.notesapp.data.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.notesapp.data.models.Note;
import com.example.notesapp.data.repository.NotesRepository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {

    private NotesRepository repository;
    private LiveData<List<Note>> allNotes;
    private LiveData<List<Note>> sortByHighPriority;
    private LiveData<List<Note>> sortByLowPriority;

    public NoteViewModel(Application application) {
        super(application);
        repository = new NotesRepository(application);
        allNotes = repository.getAllNotes();
        sortByHighPriority = repository.getAllNoteSortedByHighPriority();
        sortByLowPriority = repository.getAllNoteSortByLowPriority();
    }

    public void insert(Note note) {
       repository.insert(note);
    }

    public void update(Note note) {
       repository.update(note);
    }

    public void delete(Note note) {
       repository.delete(note);
    }

    public void deleteAll() {
       repository.deleteAll();
    }

    public LiveData<List<Note>> searchDatabase(String searchQuery) {
        return repository.searchDatabase(searchQuery);
    }

    public LiveData<List<Note>> getAllNotes(){
        return allNotes;
    }

    public LiveData<List<Note>> getAllNoteSortedByHighPriority(){
        return sortByHighPriority;
    }

    public LiveData<List<Note>> getAllNoteSortByLowPriority(){
        return sortByLowPriority;
    }

}
