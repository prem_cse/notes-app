package com.example.notesapp.fragments;

import android.view.View;
import android.widget.Spinner;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;

import com.example.notesapp.R;
import com.example.notesapp.data.models.Note;
import com.example.notesapp.data.models.Priority;
import com.example.notesapp.fragments.list.ListFragmentDirections;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static androidx.navigation.Navigation.findNavController;


public class BindingAdapters {

        @BindingAdapter("android:navigateToAddFragment")
        public static void navigateToAddFragment(FloatingActionButton view, Boolean navigate){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(navigate) findNavController(view).navigate(R.id.action_listFragment_to_addFragment);
                }
            });
        }

        @BindingAdapter("android:emptyDatabase")
        public static void emptyDatabase(View view, MutableLiveData<Boolean> emptyDatabase) {
            if(emptyDatabase.getValue()) {
                view.setVisibility(View.VISIBLE);
            }else view.setVisibility(View.INVISIBLE);
        }

        @BindingAdapter("android:parsePriorityToInt")
        public static void parsePriorityToInt(Spinner view, Priority priority){
                switch (priority) {
                    case HIGH:
                        view.setSelection(0);
                        break;
                    case MEDIUM:
                        view.setSelection(1);
                        break;
                    case LOW:
                        view.setSelection(2);
                }
        }

        @BindingAdapter("android:parsePriorityColor")
        public static void parsePriorityColor(CardView cardView, Priority priority) {
            switch (priority){
                case HIGH: cardView.setCardBackgroundColor(cardView.getContext().getColor(R.color.red));
                case MEDIUM: cardView.setCardBackgroundColor(cardView.getContext().getColor(R.color.yellow));
                case LOW: cardView.setCardBackgroundColor(cardView.getContext().getColor(R.color.green));
            }
        }

        @BindingAdapter("android:sendDataToUpdateFragment")
        public static void sendDataToUpdateFragment(ConstraintLayout view, Note currentItem){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                     ListFragmentDirections.ActionListFragmentToUpdateFragment action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem);
                     findNavController(view).navigate(action);
                }
            });
        }

}

