package com.example.notesapp.fragments;

import android.app.Application;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.notesapp.R;
import com.example.notesapp.data.models.Note;
import com.example.notesapp.data.models.Priority;

import java.util.List;


public class SharedViewModel extends AndroidViewModel {

    private static Application application;

    public SharedViewModel(Application application) {
        super(application);
        SharedViewModel.application = application;
    }

    public AdapterView.OnItemSelectedListener getListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                switch (i) {
                    case 0:
                        ((TextView) parent.getChildAt(0)).setTextColor(ContextCompat.getColor(application, R.color.red));
                        break;
                    case 1:
                        ((TextView) parent.getChildAt(0)).setTextColor(ContextCompat.getColor(application, R.color.yellow));
                        break;
                    case 2:
                        ((TextView) parent.getChildAt(0)).setTextColor(ContextCompat.getColor(application, R.color.green));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
    }

    public boolean verifyDataFromUser(String title, String description) {
        return !(title.isEmpty() || description.isEmpty());
    }

    public Priority parsePriority(String priority) {
        switch (priority){
            case "High Priority": return Priority.HIGH;
            case "Medium Priority": return Priority.MEDIUM;
            case "Low Priority": return Priority.LOW;
        }
        return Priority.LOW;
    }

    public MutableLiveData<Boolean> emptyDatabase = new MutableLiveData<>(false);

    public void checkIfDatabaseEmpty(List<Note> notes) {
        emptyDatabase.setValue(notes.isEmpty());
    }
}



