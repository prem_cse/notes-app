package com.example.notesapp.fragments.add;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.notesapp.R;
import com.example.notesapp.data.models.Note;
import com.example.notesapp.data.models.Priority;
import com.example.notesapp.data.viewmodel.NoteViewModel;
import com.example.notesapp.databinding.FragmentAddBinding;
import com.example.notesapp.fragments.SharedViewModel;

import org.jetbrains.annotations.NotNull;

import static androidx.navigation.fragment.NavHostFragment.findNavController;


public class AddFragment extends Fragment {

    private FragmentAddBinding binding;
    private final NoteViewModel noteViewModel = new ViewModelProvider(this).get(NoteViewModel.class);;
    private final SharedViewModel sharedViewModel = new ViewModelProvider(this).get(SharedViewModel.class);;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAddBinding.inflate(inflater, container, false);

        setHasOptionsMenu(true);
        binding.prioritiesSpinner.setOnItemSelectedListener(sharedViewModel.getListener());
        return binding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        inflater.inflate(R.menu.add_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
        if(item.getItemId() == R.id.menu_add){
            insertDataToDb();
        }
        return super.onOptionsItemSelected(item);
    }

    private void insertDataToDb() {
        String mTitle = binding.titleEt.getText().toString();
        String mPriority = binding.prioritiesSpinner.getSelectedItem().toString();
        String mDescription = binding.descriptionEt.getText().toString();

        boolean validation = sharedViewModel.verifyDataFromUser(mTitle, mDescription);
        if(validation){
            // Insert Data to Database
            Note newData = new Note(0, mTitle,
                    mDescription,
                    sharedViewModel.parsePriority(mPriority));

            noteViewModel.insert(newData);
            Toast.makeText(requireContext(), "Successfully added!", Toast.LENGTH_SHORT).show();
            // Navigate Back
            findNavController(this).navigate(R.id.action_addFragment_to_listFragment);
        }else{
            Toast.makeText(requireContext(), "Please fill out all fields.", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}