package com.example.notesapp.fragments.list;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.notesapp.R;
import com.example.notesapp.data.models.Note;
import com.example.notesapp.data.viewmodel.NoteViewModel;
import com.example.notesapp.databinding.FragmentAddBinding;
import com.example.notesapp.databinding.FragmentListBinding;
import com.example.notesapp.fragments.SharedViewModel;
import com.example.notesapp.fragments.list.adapter.ListAdapter;
import com.example.notesapp.utils.Utils.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.example.notesapp.utils.Utils.hideKeyboard;

public class ListFragment extends Fragment implements SearchView.OnQueryTextListener {

    private final NoteViewModel noteViewModel = new ViewModelProvider(this).get(NoteViewModel.class);
    private final SharedViewModel sharedViewModel = new ViewModelProvider(this).get(SharedViewModel.class);

    private FragmentListBinding binding;
    private ListAdapter adapter = new ListAdapter();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentListBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(this);
        binding.setMSharedViewModel(sharedViewModel);

        // Setup RecyclerView
        setupRecyclerview();

        // Observe LiveData
        noteViewModel.getAllNotes().observe(getViewLifecycleOwner(), new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                sharedViewModel.checkIfDatabaseEmpty(notes);
                adapter.setData(notes);
                binding.recyclerView.scheduleLayoutAnimation();
            }
        });

        // Set Menu
        setHasOptionsMenu(true);

        // Hide soft keyboard
        hideKeyboard(requireActivity());

        return binding.getRoot();
    }

    private void setupRecyclerview() {
        RecyclerView recyclerView = binding.recyclerView;
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_fragment_menu, menu);

        MenuItem search = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete_all : confirmRemoval();
            case R.id.menu_priority_high: noteViewModel.getAllNoteSortedByHighPriority().observe(this, notes -> adapter.setData(notes));
            case R.id.menu_priority_low: noteViewModel.getAllNoteSortByLowPriority().observe(this, notes -> adapter.setData(notes));
        }
        return super.onOptionsItemSelected(item);
    }

    private void confirmRemoval() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());

        builder.setTitle("Delete everything?");
        builder.setMessage("Are you sure you want to remove everything?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        noteViewModel.deleteAll();
                        Toast.makeText(
                                requireContext(),
                                "Successfully Removed Everything!",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        builder.create().show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query != null) {
            searchThroughDatabase(query);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        if (query != null) {
            searchThroughDatabase(query);
        }
        return true;
    }

    private void searchThroughDatabase(String query) {
        String searchQuery = "%$query%";
        noteViewModel.searchDatabase(searchQuery).observe(this, notes -> adapter.setData(notes));
    }

}
