package com.example.notesapp.fragments.list.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notesapp.data.models.Note;
import com.example.notesapp.databinding.RowLayoutBinding;


import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private List<Note> list = new ArrayList<>();

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return MyViewHolder.from(parent);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<Note> notes) {
        NoteDiffUtil diffUtil = new NoteDiffUtil(list, notes);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(diffUtil);
        list = new ArrayList<>(notes);
        result.dispatchUpdatesTo(this);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowLayoutBinding binding;

        MyViewHolder(RowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        static MyViewHolder from(ViewGroup parent){
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            RowLayoutBinding binding = RowLayoutBinding.inflate(layoutInflater, parent, false);
            return new MyViewHolder(binding);
        }

        void bind(Note note){
            binding.setNote(note);
            binding.executePendingBindings();
        }
    }
}
