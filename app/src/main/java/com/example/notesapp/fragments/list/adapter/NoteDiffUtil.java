package com.example.notesapp.fragments.list.adapter;

import androidx.recyclerview.widget.DiffUtil;

import com.example.notesapp.data.models.Note;

import java.util.List;

public class NoteDiffUtil extends DiffUtil.Callback {
    private List<Note> oldList, newList;

    public NoteDiffUtil(List<Note> oldList, List<Note> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId()
                && oldList.get(oldItemPosition).getTitle().equals(newList.get(newItemPosition).getTitle())
                && oldList.get(oldItemPosition).getDescription().equals(newList.get(newItemPosition).getTitle())
                && oldList.get(oldItemPosition).getPriority() == newList.get(newItemPosition).getPriority();
    }
}
