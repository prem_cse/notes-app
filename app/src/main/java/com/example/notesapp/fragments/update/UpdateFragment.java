package com.example.notesapp.fragments.update;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.notesapp.R;
import com.example.notesapp.data.models.Note;
import com.example.notesapp.data.viewmodel.NoteViewModel;
import com.example.notesapp.databinding.FragmentUpdateBinding;
import com.example.notesapp.fragments.SharedViewModel;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

public class UpdateFragment extends Fragment {

    private UpdateFragmentArgs args;
    private final NoteViewModel noteViewModel = new ViewModelProvider(this).get(NoteViewModel.class);
    private final SharedViewModel sharedViewModel = new ViewModelProvider(this).get(SharedViewModel.class);

    private FragmentUpdateBinding binding;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentUpdateBinding.inflate(inflater, container, false);
        binding.setArgs(args);
        // Set Menu
        setHasOptionsMenu(true);
        binding.currentPrioritiesSpinner.setOnItemSelectedListener(sharedViewModel.getListener());
        return binding.getRoot();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.update_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save: updateItem();
            case R.id.menu_delete: confirmRemoval();
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateItem() {
        String title = binding.currentTitleEt.getText().toString();
        String description = binding.currentDescriptionEt.getText().toString();
        String getPriority = binding.currentPrioritiesSpinner.getSelectedItem().toString();

        boolean validation = sharedViewModel.verifyDataFromUser(title, description);
        if (validation) {
            // Update Current Item
            Note updatedItem = new Note(
                    0,
                    title,
                    description,
                    sharedViewModel.parsePriority(getPriority));

            noteViewModel.update(updatedItem);
            Toast.makeText(requireContext(), "Successfully updated!", Toast.LENGTH_SHORT).show();
            // Navigate back
            findNavController(this).navigate(R.id.action_updateFragment_to_listFragment);
        } else {
            Toast.makeText(requireContext(), "Please fill out all fields.", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void confirmRemoval() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());

        builder.setTitle("Delete");
        builder.setMessage("Are you sure you want to remove it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        noteViewModel.delete(args.getCurrentItem());
                        Toast.makeText(
                                requireContext(),
                                "Successfully Removed !!",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        builder.create().show();
    }
}
